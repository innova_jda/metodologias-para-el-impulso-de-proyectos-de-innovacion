# METODOLOGÍA PARA IMPULSAR PROYECTOS DE INNOVACIÓN PÚBLICA: METODOLOGÍA

## Fases/pasos de la metodología

**Contenidos**: Fichas a desarrollar

### 1. Mapeando oportunidades: 

#### Identificar “oportunidades” a partir de un diagnóstico básico inicial

Lo primero que hay que hacer es un buen diagnóstico que permita identificar oportunidades genuinas de innovación. Sin un buen diagnóstico inicial corres el riesgo de intentar resolver los problemas equivocados. Para trabajar este paso del proceso, te proponemos apoyarte en **dos Fichas** que te servirán de **Herramientas de diagnóstico**:

- **F1**. Condiciones organizativas que favorecen la innovación (Cultura de innovación)
- **F2**. Áreas de oportunidades más atractivas para innovar (Tipos de innovaciones)
### 2. Eligiendo el reto:

**Elegir el “reto” de innovación que se va a acometer**

El paso anterior de “*mapear oportunidades*” te abrirá probablemente a un conjunto amplio de posibilidades para innovar, así que tendrás que descartar opciones y elegir una de ellas en la que concentrarte a partir de ahora. Es el momento de pasar de muchas “*oportunidades*” al “*reto*” concreto de innovación que vas a resolver en tu proyecto. Para poder seleccionar con criterio entre opciones, te sugerimos usar esta Ficha:

- **F3**: Criterios para evaluar y elegir retos de innovación.

### 3. Creando el equipo:

#### Crear el equipo de innovación (Ei) que liderará y diseñará el proyecto

Ya tienes el “*reto de innovación*” elegido (entre las muchas “*oportunidades*” que identificaste en el paso-1), y es el momento de pensar en las personas que necesitas para abordarlo. La innovación es un trabajo en equipo, así que es muy importante **identificar e implicar a las personas adecuadas**. Tendrás que pensar en qué tipos de roles, talentos y habilidades vas a necesitar para dar respuesta al reto elegido, y entonces buscar las personas que reúnan esas características, además de motivarlas para que se quieran implicar. La Ficha que sigue te va a servir de ayuda para crear el mejor equipo posible:

- **F4**: Test de idoneidad del equipo: ¿están todos los que son?

### 4. Investigando a fondo el reto:

#### Investigar y hacer inmersión en el reto desde un enfoque de usuario (empatía)**

Una vez que tienes el **reto** identificado, y el **equipo** que va aintentar resolverlo, hay que volver a hacer un **diagnóstico** pero está vez mucho más profundo y focalizado en el desafío que has decidido abordar. Un error muy habitual es intentar resolver un problema sin comprenderlo bien, centrar los esfuerzos en los efectos y no en las causas, o basar la solución en premisas o prejuicios equivocados. Por eso hay que hacer “*inmersión*” en el reto, mediante una concienzuda investigación que incluya trabajo de campo para la recogida de datos, con el objetivo de diagnosticar bien el problema y que eso te ayude a encontrar las soluciones más adecuadas. Este *ejercicio de inmersión* se hace por parte del equipo que aborda el reto, y para eso te **proponemos usar la metodología de Design Thinking** por lo potente que es para mejorar la empatía hacia los usuarios beneficiarios de la solución. El resultado de esta fase es un

“*Informe de Diagnóstico*” (entiéndase “informe” en un sentido laxo como unas pistas claras y documentadas que sirvan para la acción) que describe el reto mucho mejor e identifica los problemas o desafíos de fondo que se tienen que resolver. Para que puedas aplicar en los fundamentos básicos de esta metodología con cierta coherencia, vas a contar con la ayuda de estas Fichas:

- **F5**: Principios del Design Thinking
- **F6**: Algunas técnicas y herramientas del Design Thinking para el trabajo de campo

### 5. Imaginando soluciones:

#### Idear posibles soluciones para el reto con técnicas de creatividad

Ya hemos dicho que del paso anterior se sale con un “*informe de diagnóstico*” que define mejor el reto y las causas de fondo que hay que mejorar. Ahora toca pasar del diagnóstico a las propuestas de solución. Tienes mucho mejor definido (y comprendido) el desafío así que ya puedes trabajar en las posibles soluciones. Esta fase es eminentemente creativa, primero de naturaleza “*divergente*” porque el equipo tratará de exponer todas las posibilidades de solución, y después “*convergente*” porque elegirá entre esas alternativas las que mejor se adapten al reto propuesto. Para abordar con éxito esta fase de creatividad, puedes servirte de esta Ficha:

- **F7**: Técnicas de creatividad

### 6. Delimitando el proyecto:

#### Definir el proyecto de innovación a partir de las posibles soluciones generadas en el punto anterior**

Ya tienes el **reto** bien definido y la propuesta de **solución** para resolverlo, así que es el momento de concretar y documentar el **proyecto**. Lo que tenías antes no era un “*proyecto*” sino una
formulación previa menos operativa. La tarea de delimitar un proyecto necesita una concreción mucho mayor, incluyendo objetivos, plazos, plan de tareas y otras especificaciones que ayuden a la gestión. Es el proyecto el que define las pautas para la acción y sirve para coordinar la ejecución del mismo. La siguiente Ficha explica los distintos apartados que debes completar en la definición de un proyecto de innovación, así que te va a servir de guía para que no se te olvide nada importante:

- **F8**: Requisitos para la buena formulación de un proyecto de innovación

### 7. Ejecutando el Plan de Tareas:

#### Acometer las distintas tareas del proyecto siguiendo el Plan de Trabajo previsto

A partir del Plan de Tareas y de la temporalización por fases e hitos de control definidos en el paso anterior, el equipo se pone a ejecutar las tareas previstas. **Es el momento de gestionar bien para avanzar en la ejecución. Se necesita foco, eficacia y eficiencia**. El equipo tiene que estar bien coordinado y ser capaz de movilizar recursos para que las tareas se cumplan. Es bastante probable que, sobre la marcha, haga falta ajustar o actualizar el “plan de tareas” definido en el paso anterior, porque la realidad casi siempre nos depara situaciones imprevistas. La siguiente Ficha aporta algunas recomendaciones prácticas que te pueden ayudar a gestionar bien esta fase de ejecución, que es de las más importantes para conseguir resultados de impacto:

-	**F9**: Recomendaciones prácticas para la gestión de proyectos de innovación

### 8. Prototipando:

#### Bocetar soluciones en “*laboratorio*” y hacer pruebas piloto en áreas de la Admón.

-Esta fase, en realidad, podríamos incluirla en la anterior porque forma parte íntegra de la ejecución del plan de tareas del proyecto, pero hemos querido tratarla por separado dado que tiene una relevancia especial dentro del enfoque participativo y abierto que buscamos darle a esta metodología. Debe quedar claro que una premisa esencial de la búsqueda de la solución al reto consiste en “*prototipar*”, o sea, en un **ciclo iterativo de cocreación-ensayo-error-aprendizaje-cocreación** que va a ir acercando al equipo (con la participación de los usuarios o beneficiarios) a la solución más apropiada. El proceso de prototipado implica una experimentación y aprendizaje colectivo que va mucho más allá del- -“*pilotaje*” entendido en su acepción tradicional. No se trata sólo de testar una solución ya diseñada por el equipo, sino de convertir el propio proceso de prototipado en un mecanismo para la cocreación de la solución con los usuarios/beneficiarios. Tratándose de innovación pública, la mayoría de los proyectos tendrán que prototipar servicios así que viene bien que aprendas a hacer prototipos de “intangibles”. Para eso dispones de dos Fichas que te servirán de guías para esa tarea:

- **F10**: -Ideas para prototipar intangibles

### 10. Implementando y escalando:

#### Implementar la solución y difundirla para que escale en otras áreas

Una vez que la solución se ha prototipado y testado de forma iterativa, y demuestra ser una buena solución para el reto público abordado, el siguiente paso es terminar de documentarla para que pueda ser introducida en la cartera de servicios de la organización, y en otros sitios de la Administración donde tengan un problema parecido. Es convertir el resultado del proyecto en lo que llamamos habitualmente- -“buenas prácticas”, para que pueda ser replicada a una escala mayor con la colaboración de otras entidades. Este proceso de escalado también constituye una oportunidad de mejora de la solución en la medida que participan más personas y el enfoque seguido en el proyecto original se expone a una diversidad mayor de contextos que pueden enriquecerlo. Esta tarea es importante porque de ella depende que las innovaciones públicas tengan un impacto visible en la sociedad.

---
---

**Esta metodología no tiene que ser lineal**, ni se tiene que seguir rígidamente ese orden bajo cualquier circunstancia. Esos 9 pasos tienen una lógica, una coherencia, que conviene respetar; pero se suelen dar- “*interaciones*” en distintos puntos del itinerario por circunstancias en las que hay que volver hacia atrás para retomar y revisar un paso anterior.

> Si las cosas no van bien, o sospechas que vas por un camino equivocado, no dudes en volver atrás y revisar algunos de los pasos anteriores para corregir la hoja de ruta. Si en un paso previo se han tomado decisiones erróneas, eso va a influir en los siguientes, así que sé flexible en la gestión de los 9 pasos, repitiendo y corrigiendo algunos de ellos las veces que haga falta.